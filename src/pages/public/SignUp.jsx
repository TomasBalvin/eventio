import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';

import styles from './publicContent.scss';

import Input from '../../components/Input/Input';
import Button from '../../components/Button/Button';
import AccounAnnounce from '../../components/AccountAnnounce/AccountAnnouce';

import { signUp } from '../../redux/actions';
import { validate } from '../../utilities';

let SignUp = ({ isFetching, dispatch, handleSubmit }) => {
  const signUpSubmit = () => {
    const inputs = {
      firstName: document.getElementById('firstName').value,
      lastName: document.getElementById('lastName').value,
      email: document.getElementById('email').value,
      password: document.getElementById('password').value,
      password2: document.getElementById('password2').value
    };

    const error = validate(inputs);
    if (error) {
      throw new SubmissionError(error);
    }

    const user = {
      ...inputs,
      password2: undefined
    };
    return dispatch(signUp(user));
  };

  return (
    <form onSubmit={handleSubmit(signUpSubmit)}>
      <h1 className={styles.title}>Get started absolutely free.</h1>
      <p className={styles.subtitle}>Enter your details below.</p>
      <Field component={Input} label="First name" type="text" name="firstName" />
      <Field component={Input} label="Last name" type="text" name="lastName" />
      <Field component={Input} label="Email" type="email" name="email" />
      <Field component={Input} label="Password" type="password" name="password" />
      <Field component={Input} label="Repeat password" type="password" name="password2" />

      <div className={styles.accountAnnounce}>
        <AccounAnnounce />
      </div>

      <div className={styles.space} />

      <Button label="Sign up" classes={['big']} loading={isFetching} />
    </form>
  );
};

SignUp.propTypes = {
  dispatch: PropTypes.func,
  handleSubmit: PropTypes.func,
  isFetching: PropTypes.bool
};

SignUp.defaultProps = {
  dispatch: () => {},
  handleSubmit: () => {},
  isFetching: false
};

SignUp = reduxForm({
  form: 'signUpForm'
})(SignUp);

export default connect(state => ({ isFetching: state.isFetching }))(SignUp);
