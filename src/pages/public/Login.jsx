import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import classNames from 'classnames/bind';

import styles from './publicContent.scss';

import Input from '../../components/Input/Input';
import Button from '../../components/Button/Button';
import AccounAnnounce from '../../components/AccountAnnounce/AccountAnnouce';

import { login } from '../../redux/actions';
import { validate } from '../../utilities';

const cx = classNames.bind(styles);

let Login = ({ isFetching, dispatch, handleSubmit, formData }) => {
  const redirectForgotPassword = () => {
    dispatch(push('/forgot-password'));
  };

  const loginSubmit = () => {
    const inputs = {
      email: document.getElementById('email').value,
      password: document.getElementById('password').value
    };

    const error = validate(inputs);
    if (error) {
      throw new SubmissionError(error);
    }

    return dispatch(login(inputs));
  };

  const invalidForm = formData.error;
  const subtitle = invalidForm ? 'Oops! That email and pasword combination is not valid.' : 'Enter your details below.';
  const subtitleClass = cx('subtitle', { isInvalid: invalidForm });

  return (
    <form onSubmit={handleSubmit(loginSubmit)} noValidate>
      <h1 className={styles.title}>Sign in to Eventio.</h1>
      <p className={subtitleClass}>
        {subtitle}
      </p>
      <Field
        component={Input}
        label="Email" type="email"
        name="email"
      />
      <Field
        component={Input}
        label="Password"
        type="password"
        name="password"
        showPassword
      />

      <button type="button" className={styles.forgotPswd} onClick={() => redirectForgotPassword()}>
        Forgot password?
      </button>
      <div className={styles.accountAnnounce}>
        <AccounAnnounce />
      </div>

      <div className={styles.space} />

      <Button label="Sign in" classes={['big']} loading={isFetching} />
    </form>
  );
};

Login.propTypes = {
  dispatch: PropTypes.func.isRequired,
  isFetching: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  formData: PropTypes.shape({
    error: PropTypes.string
  })
};

Login.defaultProps = {
  formData: {}
};

Login = reduxForm({
  form: 'loginForm'
})(Login);

const mapStateToProps = state => ({
  isFetching: state.isFetching,
  formData: state.form.loginForm
});

export default connect(mapStateToProps)(Login);

