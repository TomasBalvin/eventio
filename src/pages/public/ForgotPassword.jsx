import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Field, reduxForm, SubmissionError } from 'redux-form';

import styles from './publicContent.scss';

import Input from '../../components/Input/Input';
import Button from '../../components/Button/Button';
import AccounAnnounce from '../../components/AccountAnnounce/AccountAnnouce';

import { requestResetLink } from '../../redux/actions';
import { validate } from '../../utilities';

class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      submitted: false
    };

    this.requestLink = this.requestLink.bind(this);
  }

  requestLink() {
    const inputs = {
      email: document.getElementById('email').value
    };

    const error = validate(inputs);
    if (error) {
      throw new SubmissionError(error);
    }
    this.props.dispatch(requestResetLink())
      .then(() => {
        this.setState({ submitted: true });
      });
  }

  render() {
    const submitted = this.state.submitted;
    const texts = {
      title: submitted ? 'Great! We sent your the reset link.' : 'Forgot your password?',
      subtitle: submitted ? 'Please check your email.' : 'Enter your email address below.'
    };

    const button = submitted
      ? <Button label="Back to homepage" classes={['big']} onClick={() => this.props.dispatch(push('/'))} />
      : <Button label="Sign up" classes={['big']} loading={this.props.isFetching} />;

    return (
      <form onSubmit={this.props.handleSubmit(this.requestLink)} noValidate>
        <h1 className={styles.title}>
          {texts.title}
        </h1>
        <p className={styles.subtitle}>
          {texts.subtitle}
        </p>

        {!submitted &&
          <div>
            <Field component={Input} label="Email" type="email" name="email" />
            <div className={styles.accountAnnounce}>
              <AccounAnnounce />
            </div>
          </div>
        }

        <div className={styles.space} />

        {button}
      </form>
    );
  }
}

ForgotPassword.propTypes = {
  dispatch: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  isFetching: PropTypes.bool.isRequired
};

const ForgotPasswordForm = reduxForm({
  form: 'forgotPasswordForm'
})(ForgotPassword);

export default connect(state => ({ isFetching: state.isFetching }))(ForgotPasswordForm);
