import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import styles from './publicContent.scss';

import Button from '../../components/Button/Button';
import AccounAnnounce from '../../components/AccountAnnounce/AccountAnnouce';

const PageDown = ({ notFound }) => {
  const title = notFound ? '404 Error - page not found' : 'Something went wrong.';
  return (
    <div>
      <h1 className={styles.title}>{title}</h1>
      <p className={styles.subtitle}>
        Seems like Darth Vader just hits our website and drops it down.
        Please press the refresh button and everything should be fine again.
      </p>

      <div className={styles.accountAnnounce}>
        <AccounAnnounce />
      </div>

      <div className={styles.space} />

      <Button label="Refresh" classes={['dark', 'big']} onClick={() => location.reload()} />
    </div>
  );
};

PageDown.propTypes = {
  notFound: PropTypes.bool
};

PageDown.defaultProps = {
  notFound: false
};

export default connect()(PageDown);
