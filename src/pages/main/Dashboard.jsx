import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classNames from 'classnames/bind';

import styles from './mainPages.scss';

import EventOverview from '../../components/Event/EventOverview';

import { fetchEvents, fetchAttendance, updateEvent } from '../../redux/actions';

const cx = classNames.bind(styles);
const FILTERS = {
  ALL: 'ALL',
  PAST: 'PAST',
  FUTURE: 'FUTURE'
};

class Dashboard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      compactView: true,
      show: FILTERS.ALL
    };
  }

  componentDidMount() {
    if (this.props.isUser) {
      this.props.dispatch(fetchEvents());
    }
  }

  renderEvents() {
    const events = [];

    const now = Date.now();
    this.props.events.forEach((event, index) => {
      const eventStart = new Date(event.startsAt).getTime();

      if (this.state.show === FILTERS.PAST && eventStart > now) {
        return;
      } else if (this.state.show === FILTERS.FUTURE && eventStart < now) {
        return;
      }

      const changeEventAttendance = actionType => (
        this.props.dispatch(fetchAttendance(actionType, event.id))
          .then((updatedEvent) => {
            this.props.dispatch(updateEvent(index, updatedEvent));
            return updatedEvent;
          })
      );

      events.push(
        <div key={event.id} className={styles.event}>
          <EventOverview event={event} compact={this.state.compactView} onAttendance={changeEventAttendance} />
        </div>
      );
    });
    return events;
  }

  render() {
    const eventsListClass = cx('eventsList', { 'eventsList-compact': this.state.compactView });
    const blockViewClass = cx('sortButton', { isActive: !this.state.compactView });
    const compactViewClass = cx('sortButton', { isActive: this.state.compactView });

    return (
      <div className={styles.dashboard}>
        <div className={styles.topBar}>
          <div className={styles['filter-mobile']}>
            <span>Show:</span>
            <select
              className={styles.select}
              name="show-filter"
              id="show-filter"
              onChange={e => this.setState({ show: e.target.value })}
            >
              <option value={FILTERS.ALL}>All events</option>
              <option value={FILTERS.FUTURE}>Future events</option>
              <option value={FILTERS.PAST}>Past events</option>
            </select>
          </div>
          <div className={styles['filter-desktop']}>
            <button
              className={cx('option', { isActive: this.state.show === FILTERS.ALL })}
              onClick={() => this.setState({ show: FILTERS.ALL })}
            >
              All events
            </button>
            <button
              className={cx('option', { isActive: this.state.show === FILTERS.FUTURE })}
              onClick={() => this.setState({ show: FILTERS.FUTURE })}
            >
              Future events
            </button>
            <button
              className={cx('option', { isActive: this.state.show === FILTERS.PAST })}
              onClick={() => this.setState({ show: FILTERS.PAST })}
            >
              Past events
            </button>
          </div>
          <div>
            <button type="button" className={blockViewClass} onClick={() => this.setState({ compactView: false })}>
              <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M0 3.8h9.5V15H0zm11.3 0h9.5V15h-9.5zm11.2 0H32V15h-9.5zM0 17h9.5v11.3H0zm11.3
                  0h9.5v11.3h-9.5zm11.2 0H32v11.3h-9.5z"
                />
              </svg>
            </button>
            <button type="button" className={compactViewClass} onClick={() => this.setState({ compactView: true })}>
              <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                <rect x="0" y="3.765" width="32" height="11.281" />
                <rect x="0" y="16.954" width="32" height="11.281" />
              </svg>
            </button>
          </div>
        </div>
        <div className={eventsListClass}>
          {this.renderEvents()}
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  dispatch: PropTypes.func,
  events: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string
    })
  ),
  isUser: PropTypes.bool
};

Dashboard.defaultProps = {
  dispatch: () => {},
  events: [],
  isUser: false
};

const mapStateToProps = state => ({
  events: state.events,
  isUser: state.user.authorization.length !== 0
});

export default connect(mapStateToProps)(Dashboard);
