import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import styles from './mainPages.scss';

import EventOverview from '../../components/Event/EventOverview';
import Attendees from '../../components/Event/Attendees';

import { fetchSingleEvent, fetchAttendance } from '../../redux/actions';

class EventDetail extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      event: false
    };

    this.changeEventAttendance = this.changeEventAttendance.bind(this);
  }

  componentDidMount() {
    this.props.dispatch(fetchSingleEvent(this.props.match.params.eventId))
      .then((response) => {
        this.setState({ event: response });
      });
  }

  changeEventAttendance(actionType) {
    return this.props.dispatch(fetchAttendance(actionType, this.state.event.id))
      .then((updatedEvent) => {
        this.setState({ event: updatedEvent });
        return updatedEvent;
      });
  }

  render() {
    return (
      <div className={styles.detailView}>
        <div className={styles.topBar}>
          <p>
            Detail event:<br />
            {this.props.match.params.eventId}
          </p>
        </div>

        {this.state.event &&
          <div className={styles.eventsList}>
            <div className={styles.event}>
              <EventOverview event={this.state.event} onAttendance={this.changeEventAttendance} />
            </div>
            <div className={styles.event}>
              <Attendees attendees={this.state.event.attendees} userId={this.props.userId} />
            </div>
          </div>
        }
      </div>
    );
  }
}

EventDetail.propTypes = {
  dispatch: PropTypes.func,
  match: PropTypes.shape({
    params: PropTypes.shape({
      eventId: PropTypes.string
    })
  }),
  userId: PropTypes.string
};

EventDetail.defaultProps = {
  dispatch: () => {},
  match: {
    params: {
      eventId: ''
    }
  },
  userId: ''
};

const mapStateToProps = (state) => {
  let formValues = {};
  const stateForm = state.form;

  if (typeof stateForm.createEventForm !== 'undefined' && typeof stateForm.createEventForm.values !== 'undefined') {
    formValues = stateForm.createEventForm.values;
  }

  return {
    formValues,
    userId: state.user.data.id
  };
};

export default connect(mapStateToProps)(EventDetail);
