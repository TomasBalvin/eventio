import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import { connect } from 'react-redux';
import { SubmissionError } from 'redux-form';
import moment from 'moment';
import { push } from 'react-router-redux';

import styles from './mainPages.scss';

import EventForm from '../../components/Event/EventForm';
import Button from '../../components/Button/Button';
import Attendees from '../../components/Event/Attendees';

import { validate } from '../../utilities';
import { fetchSingleEvent, fetchUpdateEvent, fetchDeleteEvent } from '../../redux/actions';

const cx = classNames.bind(styles);

class EditEvent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      event: false
    };

    this.updateEvent = this.updateEvent.bind(this);
    this.deleteEvent = this.deleteEvent.bind(this);
  }

  componentDidMount() {
    this.props.dispatch(fetchSingleEvent(this.props.match.params.eventId))
      .then((response) => {
        if (response.owner.id !== this.props.userId) {
          this.props.dispatch(push('/'));
        }

        const date = moment(response.startsAt);
        this.setState({ event: { ...response, date, time: date } });
      });
  }

  updateEvent() {
    const { formValues } = this.props;

    const inputs = {
      title: formValues.title,
      description: formValues.description,
      date: formValues.date,
      time: formValues.time,
      capacity: formValues.capacity
    };

    const error = validate(inputs);
    if (error) {
      throw new SubmissionError(error);
    }

    const time = inputs.time.toObject();
    const event = {
      ...inputs,
      startsAt: moment(inputs.date).add({ hours: time.hours, minutes: time.minutes }).toISOString(),
      date: undefined,
      time: undefined
    };
    return this.props.dispatch(fetchUpdateEvent(event, this.props.match.params.eventId));
  }

  deleteEvent() {
    this.props.dispatch(fetchDeleteEvent(this.props.match.params.eventId));
  }

  render() {
    const tickSvg = () => (
      <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
        <path d="M10.667,23.413l-7.414,-7.413l-2.524,2.507l9.938,9.937l21.333,-21.333l-2.507,-2.507l-18.826,18.809Z" />
      </svg>
    );

    const attendeesClass = cx('event', 'hideMobile');

    return (
      <div className={styles.detailView}>
        <div className={styles.topBar}>
          <p>
            Detail event:<br />
            {this.props.match.params.eventId}
          </p>
          <button className={styles.delete} type="button" onClick={this.deleteEvent}>
            <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M5.333,28.444c0,1.956 1.6,3.556 3.556,3.556l14.222,0c1.956,0 3.556,-1.6
                3.556,-3.556l0,-21.333l-21.334,0l0,21.333Zm23.111,-26.666l-6.222,0l-1.778,-1.778l-8.888,0l-1.778,
                1.778l-6.222,0l0,3.555l24.888,0l0,-3.555Z"
              />
            </svg>
            <span>Delete event</span>
          </button>
        </div>
        {this.state.event &&
          <div className={styles.eventsList}>
            <div className={styles.event}>
              <EventForm
                onSubmit={this.updateEvent}
                initialValues={this.state.event}
                button={<Button label="" svg={tickSvg()} classes={['circle', 'confirm', 'fixedRightBottom']} />}
              />
            </div>
            <div className={attendeesClass}>
              <Attendees attendees={this.state.event.attendees} userId={this.props.userId} />
            </div>
          </div>
        }
      </div>
    );
  }
}

EditEvent.propTypes = {
  dispatch: PropTypes.func,
  match: PropTypes.shape({
    params: PropTypes.shape({
      eventId: PropTypes.string
    })
  }),
  userId: PropTypes.string,
  formValues: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    date: PropTypes.oneOfType([PropTypes.instanceOf(moment), PropTypes.string]),
    time: PropTypes.oneOfType([PropTypes.instanceOf(moment), PropTypes.string]),
    capacity: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  })
};

EditEvent.defaultProps = {
  dispatch: () => {},
  match: {
    params: {
      eventId: ''
    }
  },
  userId: '',
  formValues: {
    title: '',
    description: '',
    date: '',
    time: '',
    capacity: ''
  }
};

const mapStateToProps = (state) => {
  let formValues = {};
  const stateForm = state.form;

  if (typeof stateForm.createEventForm !== 'undefined' && typeof stateForm.createEventForm.values !== 'undefined') {
    formValues = stateForm.createEventForm.values;
  }

  return {
    formValues,
    userId: state.user.data.id
  };
};

export default connect(mapStateToProps)(EditEvent);
