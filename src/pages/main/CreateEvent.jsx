import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SubmissionError } from 'redux-form';
import moment from 'moment';

import styles from './mainPages.scss';

import EventForm from '../../components/Event/EventForm';
import Button from '../../components/Button/Button';

import { validate } from '../../utilities';
import { fetchNewEvent } from '../../redux/actions';

const CreateEvent = ({ formValues, dispatch }) => {
  const newEventSubmit = () => {
    const inputs = {
      title: formValues.title || '',
      description: formValues.description || '',
      date: formValues.date || '',
      time: formValues.time || '',
      capacity: formValues.capacity || ''
    };

    const error = validate(inputs);
    if (error) {
      throw new SubmissionError(error);
    }

    const time = inputs.time.toObject();
    const event = {
      ...inputs,
      startsAt: moment(inputs.date).add({ hours: time.hours, minutes: time.minutes }).toISOString(),
      date: undefined,
      time: undefined
    };
    return dispatch(fetchNewEvent(event));
  };

  return (
    <div className={styles.createEvent}>
      <EventForm
        onSubmit={newEventSubmit}
        title="Create new event"
        subtitle="Enter details below"
        button={<Button label="Create new event" classes={['big']} />}
      />
    </div>
  );
};

CreateEvent.propTypes = {
  dispatch: PropTypes.func,
  formValues: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    date: PropTypes.oneOfType([PropTypes.instanceOf(moment), PropTypes.string]),
    time: PropTypes.oneOfType([PropTypes.instanceOf(moment), PropTypes.string]),
    capacity: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  })
};

CreateEvent.defaultProps = {
  dispatch: () => {},
  userId: '',
  formValues: {
    title: '',
    description: '',
    date: '',
    time: '',
    capacity: ''
  }
};

const mapStateToProps = (state) => {
  let formValues = {};
  const stateForm = state.form;

  if (typeof stateForm.createEventForm !== 'undefined' && typeof stateForm.createEventForm.values !== 'undefined') {
    formValues = stateForm.createEventForm.values;
  }

  return {
    formValues
  };
};

export default connect(mapStateToProps)(CreateEvent);
