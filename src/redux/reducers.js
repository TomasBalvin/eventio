import {
  LOGIN_SUCCESS,
  LOGOUT,
  START_FETCHING,
  STOP_FETCHING,
  SET_ERROR,
  RECEIVE_EVENTS,
  UPDATE_EVENT,
  ADD_EVENT
} from './actions';

const defaultUserState = {
  authorization: '',
  data: {}
};
const userReducer = (state = { ...defaultUserState }, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS: {
      return {
        authorization: action.authorization,
        data: { ...action.data }
      };
    }
    case LOGOUT: return { ...defaultUserState };

    default: return state;
  }
};

const fetchingReducer = (state = false, action) => {
  switch (action.type) {
    case START_FETCHING: return true;
    case STOP_FETCHING: return false;

    default: return state;
  }
};

const hasErrorReducer = (state = false, action) => {
  switch (action.type) {
    case SET_ERROR: return true;

    default: return state;
  }
};

const eventsReducer = (state = [], action) => {
  switch (action.type) {
    case RECEIVE_EVENTS: return action.events;
    case UPDATE_EVENT: {
      return [
        ...state.slice(0, action.index),
        action.event,
        ...state.slice(action.index + 1)
      ];
    }
    case ADD_EVENT: {
      return [
        ...state,
        action.event
      ];
    }

    default: return state;
  }
};

const appReducers = {
  user: userReducer,
  isFetching: fetchingReducer,
  hasError: hasErrorReducer,
  events: eventsReducer
};

export default appReducers;
