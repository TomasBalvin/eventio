import { push } from 'react-router-redux';
import { SubmissionError } from 'redux-form';

const ApiUrl = 'https://reactcontest-api.strv.com';
const ApiKey = 'k739WfxlL/fZ04yQ0m04oBivtWUmgMnfv3QY4rKskIAG';

const appHeaders = new Headers();
appHeaders.append('Content-Type', 'application/json');
appHeaders.append('APIKey', ApiKey);

export const SET_ERROR = 'SET_ERROR';
export function setError() {
  return { type: SET_ERROR };
}

export const START_FETCHING = 'START_FETCHING';
function startFetching() {
  return { type: START_FETCHING };
}

export const STOP_FETCHING = 'STOP_FETCHING';
function stopFetching() {
  return { type: STOP_FETCHING };
}

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
function finishLoginSuccess(authorization, data) {
  return { type: LOGIN_SUCCESS, authorization, data };
}

export const LOGOUT = 'LOGOUT';
export function logout() {
  return (dispatch) => {
    appHeaders.delete('Authorization');
    dispatch({ type: LOGOUT });
  };
}

export function login(user) {
  return (dispatch) => {
    dispatch(startFetching());

    const loginInit = {
      method: 'POST',
      headers: appHeaders,
      body: JSON.stringify(user)
    };

    return fetch(`${ApiUrl}/auth/native`, loginInit)
      .then((response) => {
        dispatch(stopFetching());
        if (response.ok) {
          const authorization = response.headers.get('Authorization');

          response.json().then((data) => {
            dispatch(finishLoginSuccess(authorization, data));
            dispatch(push('/dashboard'));
          });
        } else {
          throw new SubmissionError({ password: 'error', email: 'error', _error: 'error' });
        }
      }).catch((e) => {
        if (e instanceof SubmissionError) {
          throw e;
        } else {
          dispatch(setError());
        }
      });
  };
}

export function signUp(user) {
  return (dispatch) => {
    dispatch(startFetching());

    const signUpInit = {
      method: 'POST',
      headers: appHeaders,
      body: JSON.stringify(user)
    };

    return fetch(`${ApiUrl}/users`, signUpInit)
      .then((response) => {
        dispatch(stopFetching());
        if (response.ok) {
          dispatch(push('/'));
        } else {
          dispatch(setError());
        }
      }).catch(() => {
        dispatch(setError());
      });
  };
}

export function requestResetLink() {
  return (dispatch) => {
    dispatch(startFetching());

    const fakeFetch = () => new Promise(resolve => setTimeout(resolve, 2000));

    return fakeFetch()
      .then(() => {
        dispatch(stopFetching());
      });
  };
}

export const RECEIVE_EVENTS = 'RECEIVE_EVENTS';
function receiveEvents(events) {
  return { type: RECEIVE_EVENTS, events };
}

export function fetchEvents() {
  return (dispatch, getState) => {
    dispatch(startFetching());

    const state = getState();
    const { authorization } = state.user;
    appHeaders.set('Authorization', authorization);

    const allEventsInit = {
      method: 'GET',
      headers: appHeaders
    };

    return fetch(`${ApiUrl}/events`, allEventsInit)
      .then((response) => {
        dispatch(stopFetching());
        if (response.ok) {
          response.json().then((data) => {
            dispatch(receiveEvents(data));
          });
        } else if (response.status === 403) {
          dispatch(logout());
        } else {
          dispatch(setError());
        }
      }).catch(() => {
        dispatch(setError());
      });
  };
}

export const UPDATE_EVENT = 'UPDATE_EVENT';
export function updateEvent(index, event) {
  return { type: UPDATE_EVENT, index, event };
}

export function fetchAttendance(actionType, eventId) {
  return (dispatch, getState) => {
    dispatch(startFetching());

    const state = getState();
    const { authorization } = state.user;
    appHeaders.set('Authorization', authorization);

    const action = actionType === 'LEAVE' ? 'DELETE' : 'POST';
    const attendanceInit = {
      method: action,
      headers: appHeaders
    };

    return fetch(`${ApiUrl}/events/${eventId}/attendees/me`, attendanceInit)
      .then((response) => {
        dispatch(stopFetching());
        if (response.ok) {
          return response.json().then(data => (data));
        } else if (response.status === 403) {
          dispatch(logout());
        } else {
          dispatch(setError());
        }
      }).catch(() => {
        dispatch(setError());
      });
  };
}

export const ADD_EVENT = 'ADD_EVENT';
function addEvent(event) {
  return { type: ADD_EVENT, event };
}

export function fetchNewEvent(event) {
  return (dispatch, getState) => {
    dispatch(startFetching());

    const state = getState();
    const { authorization } = state.user;
    appHeaders.set('Authorization', authorization);

    const newEventInit = {
      method: 'POST',
      headers: appHeaders,
      body: JSON.stringify(event)
    };

    return fetch(`${ApiUrl}/events`, newEventInit)
      .then((response) => {
        dispatch(stopFetching());
        if (response.ok) {
          response.json().then((data) => {
            dispatch(addEvent(data));
            dispatch(push('/dashboard'));
          });
        } else if (response.status === 403) {
          dispatch(logout());
        } else {
          dispatch(setError());
        }
      }).catch(() => {
        dispatch(setError());
      });
  };
}

export function fetchSingleEvent(eventId) {
  return (dispatch, getState) => {
    dispatch(startFetching());

    const state = getState();
    const { authorization } = state.user;
    appHeaders.set('Authorization', authorization);

    const singleEventInit = {
      method: 'GET',
      headers: appHeaders
    };

    return fetch(`${ApiUrl}/events/${eventId}`, singleEventInit)
      .then((response) => {
        if (response.ok) {
          return response.json().then((data) => {
            return data;
          });
        }
      });
  };
}

export function fetchUpdateEvent(event, eventId) {
  return (dispatch, getState) => {
    dispatch(startFetching());

    const state = getState();
    const { authorization } = state.user;
    appHeaders.set('Authorization', authorization);

    const singleEventInit = {
      method: 'PATCH',
      headers: appHeaders,
      body: JSON.stringify(event)
    };

    return fetch(`${ApiUrl}/events/${eventId}`, singleEventInit)
      .then((response) => {
        dispatch(stopFetching());
        if (response.ok) {
          dispatch(push('/dashboard'));
        } else if (response.status === 403) {
          dispatch(logout());
        } else {
          dispatch(setError());
        }
      }).catch(() => {
        dispatch(setError());
      });
  };
}

export function fetchDeleteEvent(eventId) {
  return (dispatch, getState) => {
    dispatch(startFetching());

    const state = getState();
    const { authorization } = state.user;
    appHeaders.set('Authorization', authorization);

    const singleEventInit = {
      method: 'DELETE',
      headers: appHeaders
    };

    return fetch(`${ApiUrl}/events/${eventId}`, singleEventInit)
      .then((response) => {
        dispatch(stopFetching());
        if (response.ok) {
          dispatch(push('/dashboard'));
        } else if (response.status === 403) {
          dispatch(logout());
        } else {
          dispatch(setError());
        }
      }).catch(() => {
        dispatch(setError());
      });
  };
}
