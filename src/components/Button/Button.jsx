import React from 'react';
import classNames from 'classnames/bind';
import PropTypes from 'prop-types';

import styles from './button.scss';

const cx = classNames.bind(styles);

const Button = ({ label, svg, btnType, classes, loading, onClick }) => {
  const className = cx('btn', classes, { loading });

  const handleClick = (e) => {
    e.target.blur();
    onClick();
  };

  return (
    <button type={btnType && 'button'} className={className} onClick={e => handleClick(e)} disabled={loading}>
      {!loading &&
        <span>
          {label}
          {svg}
        </span>
      }
      {loading &&
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
          <rect x="48" y="43" width="4" height="14" rx="2" ry="2" fill="#fff" transform="translate(0 -36)">
            <animate attributeName="opacity" from="1" to="0" dur="1s" begin="0s" repeatCount="indefinite" />
          </rect>
          <rect x="48" y="43" width="4" height="14" rx="2" ry="2" fill="#fff" transform="rotate(45 93.456 68)">
            <animate attributeName="opacity" from="1" to="0" dur="1s" begin="0.125s" repeatCount="indefinite" />
          </rect>
          <rect x="48" y="43" width="4" height="14" rx="2" ry="2" fill="#fff" transform="rotate(90 68 68)">
            <animate attributeName="opacity" from="1" to="0" dur="1s" begin="0.25s" repeatCount="indefinite" />
          </rect>
          <rect x="48" y="43" width="4" height="14" rx="2" ry="2" fill="#fff" transform="rotate(135 57.456 68)">
            <animate attributeName="opacity" from="1" to="0" dur="1s" begin="0.375s" repeatCount="indefinite" />
          </rect>
          <rect x="48" y="43" width="4" height="14" rx="2" ry="2" fill="#fff" transform="rotate(180 50 68)">
            <animate attributeName="opacity" from="1" to="0" dur="1s" begin="0.5s" repeatCount="indefinite" />
          </rect>
          <rect x="48" y="43" width="4" height="14" rx="2" ry="2" fill="#fff" transform="rotate(-135 42.544 68)">
            <animate attributeName="opacity" from="1" to="0" dur="1s" begin="0.625s" repeatCount="indefinite" />
          </rect>
          <rect x="48" y="43" width="4" height="14" rx="2" ry="2" fill="#fff" transform="rotate(-90 32 68)">
            <animate attributeName="opacity" from="1" to="0" dur="1s" begin="0.75s" repeatCount="indefinite" />
          </rect>
          <rect x="48" y="43" width="4" height="14" rx="2" ry="2" fill="#fff" transform="rotate(-45 6.544 68)">
            <animate attributeName="opacity" from="1" to="0" dur="1s" begin="0.875s" repeatCount="indefinite" />
          </rect>
        </svg>
      }
    </button>
  );
};

Button.propTypes = {
  label: PropTypes.string.isRequired,
  classes: PropTypes.arrayOf(PropTypes.string),
  btnType: PropTypes.bool,
  onClick: PropTypes.func,
  loading: PropTypes.bool,
  svg: PropTypes.element
};

Button.defaultProps = {
  classes: [''],
  btnType: false,
  onClick: () => {},
  loading: false,
  svg: null
};

export default Button;
