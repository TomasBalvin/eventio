import React from 'react';
import classNames from 'classnames/bind';
import PropTypes from 'prop-types';
import Datetime from 'react-datetime';
import moment from 'moment';

import 'react-datetime/css/react-datetime.css';
import styles from './input.scss';

const cx = classNames.bind(styles);

class Input extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isActive: props.input.value !== '',
      type: props.type
    };
  }

  setActive() {
    this.setState({ isActive: true });
  }

  setInactive(value) {
    const inputProps = this.props.input;
    inputProps.onBlur(value);
    return value === '' && this.setState({ isActive: false });
  }

  showPassword() {
    this.setState({ type: 'text' });
  }

  hidePassword() {
    this.setState({ type: 'password' });
  }

  renderDataInput() {
    const inputProps = this.props.input;
    const isInFuture = date => (moment().subtract(1, 'days').diff(date) < 0);

    const commonSettings = {
      ...inputProps,
      onFocus: () => this.setActive(),
      onBlur: () => this.setInactive(inputProps.value)
    };

    const identification = {
      id: inputProps.name,
      name: inputProps.name
    };

    if (this.props.type === 'date') {
      return (
        <Datetime
          {...commonSettings}
          inputProps={identification}
          isValidDate={isInFuture}
          dateFormat="MMMM D, YYYY"
          timeFormat={false}
        />
      );
    } else if (this.props.type === 'time') {
      return (
        <Datetime
          {...commonSettings}
          inputProps={identification}
          dateFormat={false}
        />
      );
    }

    return (
      <input
        {...commonSettings}
        {...identification}
        type={this.state.type}
      />
    );
  }

  render() {
    const inputProps = this.props.input;
    const metaProps = this.props.meta;

    const className = cx('inputGroup', this.props.classes, {
      isActive: this.state.isActive,
      isInvalid: metaProps.touched && metaProps.error
    });

    return (
      <div className={className}>
        <label htmlFor={inputProps.name} className={styles.label}>{this.props.label}</label>
        {this.renderDataInput()}
        {this.props.showPassword && this.state.isActive &&
          <button
            type="button"
            className={styles.showPassword}
            onMouseDown={() => this.showPassword()}
            onMouseUp={() => this.hidePassword()}
            onBlur={() => this.hidePassword()}
            onMouseLeave={() => this.hidePassword()}
          >
            <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" fillRule="evenodd">
              <path
                d="M.06 15.87c1.7-4.17 5-7.67 9.46-9.5C18.34 2.8 28.38 7.1 31.94 16l.05.16c-1.7 4.17-5 7.66-9.48
                9.48-8.8 3.6-18.86-.72-22.4-9.62l-.07-.15zM16 8.74c4 0 7.26 3.25 7.26 7.26 0 4-3.25 7.26-7.26 7.26-4
                0-7.26-3.25-7.26-7.26 0-4 3.25-7.26 7.26-7.26zm0 2.9c2.4 0 4.35 1.96 4.35 4.36 0 2.4-1.95 4.35-4.35
                4.35-2.4 0-4.35-1.95-4.35-4.35 0-2.4 1.95-4.35 4.35-4.35z"
              />
            </svg>
          </button>
        }
        {metaProps.touched && metaProps.error && metaProps.error !== 'error' &&
          <span className={styles.error}>{metaProps.error}</span>
        }
      </div>
    );
  }
}

Input.propTypes = {
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  classes: PropTypes.arrayOf(PropTypes.string),
  showPassword: PropTypes.bool,

  input: PropTypes.shape({
    name: PropTypes.string,
    onBlur: PropTypes.func,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.instanceOf(moment)
    ])
  }).isRequired,

  meta: PropTypes.shape({
    error: PropTypes.string,
    touched: PropTypes.bool
  }).isRequired
};

Input.defaultProps = {
  classes: [''],
  showPassword: false
};

export default Input;
