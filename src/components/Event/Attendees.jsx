import React from 'react';
import classNames from 'classnames/bind';
import PropTypes from 'prop-types';

import styles from './event.scss';

const cx = classNames.bind(styles);

const Attendees = ({ attendees, userId }) => {
  const userClass = cx('attendee', 'isUser');

  const renderAttendees = () => {
    const attendeesArray = [];

    attendees.forEach((attendee) => {
      if (attendee.id === userId) {
        attendeesArray.unshift(
          <div key={attendee.id} className={userClass}>You</div>
        );
      } else {
        attendeesArray.push(
          <div key={attendee.id} className={styles.attendee}>{attendee.firstName} {attendee.lastName}</div>
        );
      }
    });

    return attendeesArray;
  };

  return (
    <section className={styles.eventBox}>
      <h2 className={styles.title}>Attendees</h2>
      <div className={styles.attendeesList}>
        {renderAttendees()}
      </div>
    </section>
  );
};

Attendees.propTypes = {
  attendees: PropTypes.arrayOf(PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string
  })).isRequired,
  userId: PropTypes.string.isRequired
};

Attendees.defaultProps = {
  attendees: []
};

export default Attendees;
