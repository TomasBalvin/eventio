import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';

import styles from './event.scss';

import Input from '../Input/Input';

const cx = classNames.bind(styles);

const CreateEvent = ({ title, subtitle, button, onSubmit, handleSubmit }) => {
  const eventBoxClass = cx('eventBox', 'eventBox-form');

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={eventBoxClass} noValidate>
      {title && <h1 className={styles.title}>{title}</h1>}
      {subtitle && <p className={styles.subtitle}>{subtitle}</p>}
      <Field component={Input} label="Title" type="text" name="title" />
      <Field component={Input} label="Description" type="text" name="description" />
      <Field component={Input} label="Date" type="date" name="date" />
      <Field component={Input} label="Time" type="time" name="time" />
      <Field component={Input} label="Capacity" type="number" name="capacity" />
      <div className={styles.buttonSpace} />
      {button}
    </form>
  );
};

CreateEvent.propTypes = {
  handleSubmit: PropTypes.func,
  button: PropTypes.element.isRequired,
  title: PropTypes.string,
  subtitle: PropTypes.string,
  onSubmit: PropTypes.func.isRequired
};

CreateEvent.defaultProps = {
  dispatch: () => {},
  handleSubmit: () => {},
  title: '',
  subtitle: ''
};

const CreateEventFrom = reduxForm({
  form: 'createEventForm'
})(CreateEvent);

export default connect()(CreateEventFrom);
