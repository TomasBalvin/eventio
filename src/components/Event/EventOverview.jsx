import React from 'react';
import classNames from 'classnames/bind';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';
import { push } from 'react-router-redux';

import styles from './event.scss';

import Button from '../Button/Button';

const cx = classNames.bind(styles);

class EventOverview extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isFetching: false,
      event: props.event
    };

    this.changeAttendance = this.changeAttendance.bind(this);
    this.redirectToEdit = this.redirectToEdit.bind(this);
  }

  getReadableDate() {
    const date = new Date(this.props.event.startsAt);
    return moment(date).format('MMMM D, YYYY - h:m A');
  }

  changeAttendance(actionType) {
    this.setState({ isFetching: true });

    this.props.onAttendance(actionType)
      .then((updatedEvent) => {
        this.setState({ isFetching: false, event: updatedEvent });
      });
  }

  redirectToEdit() {
    this.props.dispatch(push(`/edit/${this.props.event.id}`));
  }

  renderActionButton() {
    const loading = this.props.isFetching && this.state.isFetching;

    if (this.props.isOwner) {
      return (
        <Button label="Edit" classes={['edit']} onClick={this.redirectToEdit} />
      );
    } else if (this.props.isAttendee) {
      return (
        <Button label="Leave" loading={loading} classes={['delete']} onClick={() => this.changeAttendance('LEAVE')} />
      );
    }

    return (
      <Button label="Join" loading={loading} onClick={() => this.changeAttendance('JOIN')} />
    );
  }

  render() {
    const { compact } = this.props;
    const { event } = this.state;

    const eventBoxClass = cx('eventBox', { 'eventBox-compact': compact });
    const overviewClass = cx('overview', { 'overview-compact': compact });

    return (
      <section className={eventBoxClass}>
        <div className={overviewClass}>
          <p className={styles.date}>{this.getReadableDate()}</p>
          <h2 className={styles.title}>
            <button onClick={() => this.props.dispatch(push(`/detail/${event.id}`))}>
              {event.title}
            </button>
          </h2>
          <p className={styles.author}>{`${event.owner.firstName} ${event.owner.lastName}`}</p>
          <p className={styles.description}>{event.description}</p>
          <div className={styles.attendees}>
            {!compact && <i>man</i>}
            <span>{`${event.attendees.length} of ${event.capacity}`}</span>
          </div>

          <div className={styles.actionButton}>
            {this.renderActionButton()}
          </div>
        </div>
      </section>
    );
  }
}

EventOverview.propTypes = {
  dispatch: PropTypes.func,
  compact: PropTypes.bool,
  event: PropTypes.shape({
    id: PropTypes.string,
    startsAt: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string,
    capacity: PropTypes.number,
    attendees: PropTypes.array,
    owner: PropTypes.shape({
      id: PropTypes.string,
      firstName: PropTypes.string,
      lastName: PropTypes.string
    })
  }).isRequired,
  isFetching: PropTypes.bool,
  isOwner: PropTypes.bool,
  isAttendee: PropTypes.bool,
  onAttendance: PropTypes.func.isRequired
};

EventOverview.defaultProps = {
  dispatch: () => {},
  compact: false,
  isFetching: false,
  isOwner: false,
  isAttendee: false
};

const mapStateToProps = (state, ownProps) => {
  const userId = state.user.data.id;
  const isOwner = ownProps.event.owner.id === userId;

  const isAttendee = ownProps.event.attendees.some(attendee => (
    attendee.id === userId
  ));

  return {
    isOwner,
    isAttendee,
    isFetching: state.isFetching
  };
};

export default connect(mapStateToProps)(EventOverview);
