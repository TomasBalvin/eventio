import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import styles from './accountAnnounce.scss';

const AccountAnnounce = ({ location, dispatch }) => {
  const signUp = {
    question: 'Don’t have account?',
    action: 'Sign up',
    path: '/sign-up'
  };

  const signIn = {
    question: 'Already have an account?',
    action: 'Sign in',
    path: '/'
  };

  const option = location === '/sign-up' ? signIn : signUp;

  const handleClick = (btn) => {
    btn.blur();
    dispatch(push(option.path));
  };

  return (
    <div className={styles.announce}>
      <span>{option.question}</span>
      <button type="button" onClick={e => handleClick(e.target)}>{option.action}</button>
    </div>
  );
};

AccountAnnounce.propTypes = {
  location: PropTypes.string,
  dispatch: PropTypes.func
};

AccountAnnounce.defaultProps = {
  location: '/',
  dispatch: () => {}
};

const mapStateToProps = state => ({
  location: state.router.location.pathname
});

export default connect(mapStateToProps)(AccountAnnounce);
