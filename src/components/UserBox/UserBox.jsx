import React from 'react';
import { findDOMNode } from 'react-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import { connect } from 'react-redux';

import styles from './userBox.scss';

import { logout } from '../../redux/actions';

const cx = classNames.bind(styles);

class UserBox extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    };

    this.toggleOpen = this.toggleOpen.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  componentDidMount() {
    document.addEventListener('click', this.handleClickOutside, true);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside, true);
  }

  handleClickOutside(e) {
    const domNode = findDOMNode(this);

    if ((!domNode || !domNode.contains(e.target))) {
      this.setState({ isOpen: false });
    }
  }

  toggleOpen() {
    this.setState(prevState => ({
      isOpen: !prevState.isOpen
    }));
  }

  render() {
    const { firstName, lastName, dispatch } = this.props;

    const boxClass = cx('userBox', {
      isOpen: this.state.isOpen
    });

    return (
      <div className={boxClass}>
        <div className={styles.profilePicture}>
          <span>{firstName[0] + lastName[0]}</span>
        </div>
        <span className={styles.name}>{`${firstName} ${lastName}`}</span>
        <button type="button" className={styles.detailsToggle} onClick={this.toggleOpen}>
          <div />
        </button>
        <div className={styles.details}>
          <button type="button">Profile</button>
          <button type="button" onClick={() => dispatch(logout())}>Log out</button>
          <svg viewBox="0 0 29 12" xmlns="http://www.w3.org/2000/svg">
            <path d="M14.5 0c5 0 9 12 14 12H.5c5 0 9-12 14-12z" />
          </svg>
        </div>
      </div>
    );
  }
}

UserBox.propTypes = {
  dispatch: PropTypes.func,
  firstName: PropTypes.string,
  lastName: PropTypes.string
};

UserBox.defaultProps = {
  dispatch: () => {},
  firstName: '',
  lastName: ''
};

const mapStateToProps = state => ({
  firstName: state.user.data.firstName,
  lastName: state.user.data.lastName
});

export default connect(mapStateToProps)(UserBox);
