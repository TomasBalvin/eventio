import 'babel-polyfill';
import React from 'react';
import ReactDom from 'react-dom';

import ReduxThunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { reducer as formReducer } from 'redux-form';

import createHistory from 'history/createBrowserHistory';

import { Switch } from 'react-router-dom';
import { ConnectedRouter, routerReducer, routerMiddleware } from 'react-router-redux';

import moment from 'moment';
import jwtDecode from 'jwt-decode';
import appReducers from './redux/reducers';
import './main.scss';
import { saveState, loadState } from './utilities';

import PublicLayout from './layouts/Public/PublicLayout';
import Login from './pages/public/Login';
import SignUp from './pages/public/SignUp';
import ForgotPassword from './pages/public/ForgotPassword';
import PageDown from './pages/public/PageDown';

import MainLayout from './layouts/Main/MainLayout';
import Dashboard from './pages/main/Dashboard';
import CreateEvent from './pages/main/CreateEvent';
import EditEvent from './pages/main/EditEvent';
import EventDetail from './pages/main/EventDetail';

const history = createHistory();
const historyMiddleware = routerMiddleware(history);


const persistedState = loadState();

if (persistedState && persistedState.user.authorization) {
  if (jwtDecode(persistedState.user.authorization).exp < moment().unix()) {
    persistedState.user = undefined;
  }
}

const store = createStore(
  combineReducers({
    ...appReducers,
    router: routerReducer,
    form: formReducer
  }),
  persistedState,
  compose(
    applyMiddleware(ReduxThunk),
    applyMiddleware(historyMiddleware)
  )
);

ReactDom.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Switch>
        <PublicLayout exact path="/" component={Login} />
        <PublicLayout path="/sign-up" component={SignUp} />
        <PublicLayout path="/forgot-password" component={ForgotPassword} />
        <MainLayout path="/dashboard" component={Dashboard} displayCreateButton />
        <MainLayout path="/create" component={CreateEvent} />
        <MainLayout path="/edit/:eventId" component={EditEvent} isPadded />
        <MainLayout path="/detail/:eventId" component={EventDetail} displayCreateButton />
        <PublicLayout component={PageDown} notFound hasBackground />
      </Switch>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);

const authorization = store.getState().user.authorization;
store.subscribe(() => {
  const state = store.getState();
  if (state.user.authorization !== authorization) {
    saveState({
      user: state.user
    });
  }
});
