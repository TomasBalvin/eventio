import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router';
import classNames from 'classnames/bind';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import styles from './mainLayout.scss';

import PublicLayout from '../Public/PublicLayout';
import PageDown from '../../pages/public/PageDown';
import Button from '../../components/Button/Button';
import UserBox from '../../components/UserBox/UserBox';

const cx = classNames.bind(styles);

class MainLayout extends React.Component {
  componentDidMount() {
    this.handleUserSatus();
  }

  componentDidUpdate() {
    this.handleUserSatus();
  }

  handleUserSatus() {
    if (!this.props.isUser) {
      this.props.dispatch(push('/'));
    }
  }

  render() {
    const { component: Component, hasError, dispatch, isPadded, displayCreateButton, ...rest } = this.props;
    const containerClass = cx('container', { isPadded: isPadded || displayCreateButton });

    return (
      <div>
        {!hasError
          ? <Route
            {...rest}
            render={matchProps => (
              <div className={containerClass}>
                <header className={styles.header}>
                  <button type="button" className={styles.logo} onClick={() => dispatch(push('/'))}>
                    <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                      <path
                        d="M0 31.02V.68h19v5.57H5.9v6.93h11.85v5.26H5.9v7H19v5.58H0zm24.17-3.6c0-1.08.38-2
                      1.13-2.76.76-.75 1.68-1.13 2.76-1.13.55 0 1.06.1 1.54.3.5.2.9.48 1.27.83.35.36.63.77.83
                      1.24.2.47.3.98.3 1.52s-.1 1.05-.3 1.52c-.2.47-.48.88-.83 1.24-.36.36-.78.64-1.27.84-.48.2-1
                      .3-1.54.3-1.08 0-2-.38-2.75-1.14-.74-.75-1.12-1.67-1.12-2.76z"
                      />
                    </svg>
                  </button>

                  <UserBox />

                </header>
                <main className={styles.main}>
                  <Component {...matchProps} />
                </main>

                {displayCreateButton &&
                  <div className={styles.addButton}>
                    <Button label="+" classes={['add', 'dark', 'circle']} onClick={() => dispatch(push('/create'))} />
                  </div>
                }
              </div>
            )}
          />
          : <PublicLayout component={PageDown} />
        }
      </div>
    );
  }
}

MainLayout.propTypes = {
  dispatch: PropTypes.func,
  component: PropTypes.func.isRequired,
  hasError: PropTypes.bool,
  isUser: PropTypes.bool,
  displayCreateButton: PropTypes.bool,
  isPadded: PropTypes.bool
};

MainLayout.defaultProps = {
  dispatch: () => {},
  hasError: false,
  isUser: false,
  displayCreateButton: false,
  isPadded: false
};

const mapStateToProps = state => ({
  hasError: state.hasError,
  isUser: state.user.authorization !== ''
});

export default connect(mapStateToProps)(MainLayout);
