import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router';
import classNames from 'classnames/bind';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import styles from './publicLayout.scss';

import AccountAnnounce from '../../components/AccountAnnounce/AccountAnnouce';
import PageDown from '../../pages/public/PageDown';

const cx = classNames.bind(styles);

class PublicLayout extends React.Component {
  componentDidMount() {
    if (this.props.isUser && !this.props.notFound && !this.props.hasError) {
      this.props.dispatch(push('/dashboard'));
    }
  }

  render() {
    const { component: Component, hasBackground, hasError, dispatch, ...rest } = this.props;

    const mainClass = cx('main', {
      trooperBg: hasBackground
    });

    return (
      <Route
        {...rest}
        render={matchProps => (
          <div className={styles.container}>
            <button type="button" className={styles.logo} onClick={() => dispatch(push('/'))}>
              <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M0 31.02V.68h19v5.57H5.9v6.93h11.85v5.26H5.9v7H19v5.58H0zm24.17-3.6c0-1.08.38-2 1.13-2.76.76-.75
                1.68-1.13 2.76-1.13.55 0 1.06.1 1.54.3.5.2.9.48 1.27.83.35.36.63.77.83 1.24.2.47.3.98.3 1.52s-.1 1.05-.3
                1.52c-.2.47-.48.88-.83 1.24-.36.36-.78.64-1.27.84-.48.2-1 .3-1.54.3-1.08
                0-2-.38-2.75-1.14-.74-.75-1.12-1.67-1.12-2.76z"
                />
              </svg>
            </button>
            <aside className={styles.sidePanel}>
              <blockquote>
                <h2 className={styles.quote}>“Great, kid. Don’t get cocky.”</h2>
                <div className={styles.delimiter} />
                <p className={styles.author}>Han Solo</p>
              </blockquote>
            </aside>
            <main className={mainClass}>
              <div className={styles.accountAnnounce}>
                <AccountAnnounce />
              </div>
              <div className={styles.pageContent}>
                {!hasError ? <Component {...matchProps} notFound={rest.notFound} /> : <PageDown />}
              </div>
            </main>
          </div>
        )}
      />
    );
  }
}

PublicLayout.propTypes = {
  dispatch: PropTypes.func,
  component: PropTypes.func.isRequired,
  hasBackground: PropTypes.bool,
  hasError: PropTypes.bool,
  isUser: PropTypes.bool,
  notFound: PropTypes.bool
};

PublicLayout.defaultProps = {
  dispatch: () => {},
  hasBackground: false,
  hasError: false,
  isUser: false,
  notFound: false
};

const mapStateToProps = state => ({
  isUser: state.user.authorization !== '',
  hasError: state.hasError
});

export default connect(mapStateToProps)(PublicLayout);
