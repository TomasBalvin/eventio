import moment from 'moment';

const isEmail = email => (
  /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)
);

export const validate = (values) => {
  const errors = {};

  Object.entries(values).forEach(([key, value]) => {
    if (!value) {
      errors[key] = 'This field is required';
    } else if (key === 'email' && !isEmail(value)) {
      errors[key] = 'Invalid email address';
    } else if (key === 'password2' && value !== values.password) {
      errors[key] = 'Passwords does not match';
    } else if (key === 'capacity' && (value <= 1 || value % 1 !== 0)) {
      errors[key] = 'Capacity must be whole number bigger than 1';
    } else if (key === 'date' && !moment(value).isValid()) {
      errors[key] = 'Date must be in correct format';
    } else if (key === 'time' && !moment(value).isValid()) {
      errors[key] = 'Time must be in correct format';
    }
  });

  return Object.keys(errors).length === 0 ? false : errors;
};

export const saveState = (state) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('state', serializedState);
  } catch (error) {
    // Ignore
  }
};

export const loadState = () => {
  try {
    const serializedState = localStorage.getItem('state');
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
};
