const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const WebpackChunkHash = require('webpack-chunk-hash');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');
const postcssImport = require('postcss-import');

module.exports = (env) => {
  const devtool = env === 'dev' ? 'inline-source-map' : 'source-map';

  const config = {
    entry: {
      app: './src/index.jsx'
    },
    output: {
      path: path.join(__dirname, 'public'),
      publicPath: '/',
      filename: env === 'dev' ? '[name].js' : '[name].[chunkhash].js',
      chunkFilename: env === 'dev' ? '[name].js' : '[name].[chunkhash].js',
      sourceMapFilename: env === 'dev' ? '[name].map' : '[name].[chunkhash].map'
    },
    resolve: {
      extensions: ['.js', '.jsx']
    },
    devtool,
    module: {
      rules: [{
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader?cacheDirectory'
        }
      }, {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          filename: '[name].[contenthash].css',
          use: [{
            loader: 'css-loader',
            options: {
              sourceMap: true,
              modules: true,
              localIdentName: '[name]_[local]-[hash:base64:10]',
              minimize: env !== 'dev'
            }
          }, {
            loader: 'postcss-loader',
            options: {
              plugins: () => ([
                autoprefixer,
                postcssImport
              ])
            }
          }, {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          }]
        })
      }, {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' }
        ]
      }, {
        test: /\.ttf$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '/fonts/[name].[ext]'
          }
        }
      }, {
        test: /\.(jpg|png|svg)$/,
        exclude: /favicons/,
        use: {
          loader: 'file-loader',
          options: {
            name: './img/[name].[hash].[ext]'
          }
        }
      }, {
        test: /\.(png|ico|json|xml)$/,
        include: /favicons/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name].[hash].[ext]'
          }
        }
      }, {
        test: /\.html$/,
        use: [{
          loader: 'html-loader',
          query: {
            interpolate: 'require'
          }
        }]
      }]
    },
    plugins: [
      new webpack.optimize.CommonsChunkPlugin({
        name: 'vendor',
        minChunks: module => (
          module.context && module.context.indexOf('node_modules') !== -1
        )
      }),
      new webpack.optimize.CommonsChunkPlugin({
        names: 'manifest',
        minChunks: Infinity
      }),
      new WebpackChunkHash(),
      new ExtractTextPlugin({
        filename: env === 'dev' ? 'main.css' : 'main.[chunkhash].css',
        allChunks: true
      }),
      new HtmlWebpackPlugin({
        template: 'src/index.ejs'
      })
    ],
    devServer: {
      contentBase: path.join(__dirname, ''),
      compress: true,
      port: 9000,
      historyApiFallback: true
    }
  };

  if (env === 'dev') {
    config.plugins.push(new webpack.NamedModulesPlugin());
  } else {
    config.plugins.push(new webpack.HashedModuleIdsPlugin());
  }

  return config;
};
